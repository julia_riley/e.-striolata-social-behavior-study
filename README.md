This is the information file for the data and code used in the paper **"Isolation rearing does not constrain social plasticity in a family-living lizard"**. This data has also been desposited within Dryad (without the R Code) at doi:10.5061/dryad.bn628.

Any comments or questions, please contact Julia Riley: julia.riley87@gmail.com

**The following files are provided in the 'data' folder:**

1. Group Data_long.csv - data on social association over our six month observations
2. Group Data_short.csv - data on social association over our six week observations
3. Lizard_Attributes_long.csv - lizard attribute data for the six month observations
4. Lizard_Attributes_short.csv - lizard attribute data for the six week observations
5. groupbyindividual_long.csv - worksheet to organise network data by time-period (six month observations)
6. groupbyindividual_short.csv - worksheet to organise network data by time-period (six week observations)
7. personality_data_NArm.csv - final sociability data for analyses
8. Metadata for Datasets.rtf - description of each column within each dataset (.csv file) listed above

**The following files are provided in the 'script' folder & are for the following analyses steps:**

*Data Preparation*

1. LAB_social data prep.R - organising & selecting data for lab sociability analyses

*Statistical Analyses*

2. LAB_social models.R - lab sociability analyses
3. long_temporal change.R - social network analysis for our six month observations
4. short_temporal change.R - social network analysis for our six week observations
5. group size and sex.R - summary of tree skink groups

*Figures*

6. Repeatibility Fig2.R - code for Figure 2

**These additional files are within the main repository:**

1. Final.Rproj - the Rstudio project for this analyses
2. .gitignore - the ignore file
3. README.md - details about the project (this file)
